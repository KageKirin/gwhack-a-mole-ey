using UnityEngine;
using UnityEditor;
using System;

/// <summary>
/// Mole field data editor.
/// script to add mole field data asset creation to editor
/// </summary>
public class MoleFieldDataEditor
{
	[MenuItem("Assets/Create/MoleFieldData")]
	public static void CreateMoleFieldData()
	{
		var asset = new MoleFieldData();
		var assetPath = AssetDatabase.GenerateUniqueAssetPath("Assets/MoleData/New MoleField.asset");
		Debug.Log("generated path: " + assetPath);
		AssetDatabase.CreateAsset(asset, assetPath);
		AssetDatabase.SaveAssets();
		EditorUtility.FocusProjectWindow();
		Selection.activeObject = asset;
		// Print the path of the created asset
		Debug.Log(AssetDatabase.GetAssetPath(asset));
	}
}