using UnityEngine;
using UnityEditor;
using System;

/// <summary>
/// Mole set editor.
/// script to add mole set asset creation to editor
/// </summary>
public class MoleSetEditor
{
	[MenuItem("Assets/Create/MoleSet")]
	public static void CreateMoleSet()
	{
		MoleSet asset = new MoleSet();
		var assetPath = AssetDatabase.GenerateUniqueAssetPath("Assets/MoleData/New MoleSet.asset");
		Debug.Log("generated path: " + assetPath);
		AssetDatabase.CreateAsset(asset, assetPath);
		AssetDatabase.SaveAssets();
		EditorUtility.FocusProjectWindow();
		Selection.activeObject = asset;
		// Print the path of the created asset
		Debug.Log(AssetDatabase.GetAssetPath(asset));
	}
}