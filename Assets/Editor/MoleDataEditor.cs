using UnityEngine;
using UnityEditor;
using System;

/// <summary>
/// Mole data editor.
/// script to add mole data asset creation to editor
/// </summary>
public class MoleDataEditor
{
	[MenuItem("Assets/Create/MoleData")]
	public static void CreateMoleData()
	{
		MoleData asset = new MoleData();
		var assetPath = AssetDatabase.GenerateUniqueAssetPath("Assets/MoleData/New MoleData.asset");
		Debug.Log("generated path: " + assetPath);
		AssetDatabase.CreateAsset(asset, assetPath);
		AssetDatabase.SaveAssets();
		EditorUtility.FocusProjectWindow();
		Selection.activeObject = asset;
		// Print the path of the created asset
		Debug.Log(AssetDatabase.GetAssetPath(asset));
	}
}