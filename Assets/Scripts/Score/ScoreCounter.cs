using System;

public class ScoreCounter
{
	private ScoreData _score = new ScoreData();
	public ScoreData Score { get { return _score; } }

	public virtual void OnScored(System.Object sender, EventArgs e)
	{
		_score.Points++;
	}
}
