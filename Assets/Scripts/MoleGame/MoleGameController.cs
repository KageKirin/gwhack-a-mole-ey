﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Mole game controller.
/// controls the mole game
/// </summary>
public class MoleGameController : MonoBehaviour
{
	public MoleManager MoleManagerInstance;
	private ScoreCounter _scoreCounter;

	public ScoreDisplay ScoreDisplayInstance;
	public TimeDisplay TimeDisplayInstance;

	public Timer GameTimer;
	public Timer SpawnTimer;

	// Use this for initialization
	void Start()
	{
		_scoreCounter = new ScoreCounter();
		MoleManagerInstance.Scored += _scoreCounter.OnScored;
		MoleManagerInstance.Scored += OnScored;
		MoleManagerInstance.Filled += OnMoleManagerFilled;

		GameTimer.Finished += OnGameTimerFinished;
		SpawnTimer.Finished += OnSpawnTimerFinished;

		MoleManagerInstance.SpawnMole();
	}

	protected virtual void OnScored(System.Object sender, EventArgs e)
	{
		ScoreDisplayInstance.Score = _scoreCounter.Score.Points;
	}

	protected virtual void OnSpawnTimerFinished(System.Object sender, EventArgs e)
	{
		MoleManagerInstance.SpawnMole();
		SpawnTimer.Reset();
	}

	protected virtual void OnGameTimerFinished(System.Object sender, EventArgs e)
	{
		EndGame();
	}

	protected virtual void OnMoleManagerFilled(System.Object sender, EventArgs e)
	{
		EndGame();
	}

	private void EndGame()
	{
		Application.LoadLevel("GameOverScreen");
	}

	// Update is called once per frame
	void Update()
	{
		TimeDisplayInstance.Time = (uint)GameTimer.RemainingTime;
	}
}
