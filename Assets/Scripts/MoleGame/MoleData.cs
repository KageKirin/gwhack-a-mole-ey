﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Mole data.
/// holds the data respective to one mole (type) in a serializable fashion
/// </summary>
public class MoleData : ScriptableObject
{
	/// <summary>
	/// mesh used for display
	/// </summary>
	public Mesh model = null;

	/// <summary>
	/// time moving in
	/// </summary>
	public float moveInTime = 1.0f;

	/// <summary>
	/// move in curve
	/// </summary>
	public AnimationCurve moveInCurve;

	/// <summary>
	/// display time
	/// </summary>
	public float displayTime = 1.0f;

	/// <summary>
	/// display curve
	/// </summary>
	public AnimationCurve displayCurve;

	/// <summary>
	/// time moving out
	/// </summary>
	public float moveOutTime = 1.0f;

	/// <summary>
	/// move out curve
	/// </summary>
	public AnimationCurve moveOutCurve;

	/// <summary>
	/// life points, decremented on tap
	/// </summary>
	public int hitPoints = 1;

	/// <summary>
	/// mole speed
	/// units /s
	/// </summary>
	public float speed = 1.0f;

	/// <summary>
	/// random movement arc.
	/// random movement limited to this arc
	/// in degrees.
	/// </summary>
	public float randomMovementArc;
}
