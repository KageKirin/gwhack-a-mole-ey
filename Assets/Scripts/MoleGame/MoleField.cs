﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// data structure for mole field
/// TODO: export, make serializable?
/// </summary>
public struct MoleFieldDataStruct
{
	[SerializeField]
	public Vector3		position;

	[SerializeField]
	public GameObject	mole;
}


//consider using this instead: http://arbel.net/2004/07/07/the-hidden-c-typedef/
//using MoleField = System.Collections.Generic.Dictionary<FieldID, MoleFieldDataStruct>;

public class MoleField : Dictionary<FieldID, MoleFieldDataStruct>
{
	public int Capacity { get; private set; }


	public MoleField(int capacity)
	: base(capacity)
	{
		Capacity = capacity;

		for (int i = 0; i < capacity; i++)
		{
			Add(new FieldID(i), new MoleFieldDataStruct());
		}
	}

	public bool MoleFull()
	{
		return (Count == Capacity);
	}

	public bool MoleEmpty()
	{
		return false;
		//Where(kvp => kvp.Value.mole == null)
		//.Count() == 0;
		//System.Linq.Any(base, kvp => kvp.Value.mole == null);
	}

	public FieldID GetRandomEmpty(System.Random random)
	{
		var fID = new FieldID(0);
		MoleFieldDataStruct mfd = this[fID];

		while (mfd.mole != null)
		{
			var rnd = random.Next(0, Count);
			fID = new FieldID(rnd);
			mfd = this[fID];
		}

		return fID;
	}
}
