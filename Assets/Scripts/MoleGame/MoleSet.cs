﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Mole set.
/// holds a set of MoleData
/// </summary>
public class MoleSet : ScriptableObject
{
	/// <summary>
	/// set of moles
	/// </summary>
	public List<GameObject> moles;
}
