using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
using DEBUG = System.Diagnostics.Debug;

using OccupancyLUT = System.Collections.Generic.Dictionary<UnityEngine.Vector3, bool>;
using MolePositionLUT = System.Collections.Generic.Dictionary<UnityEngine.Vector3, UnityEngine.GameObject>;

public class MoleFieldController
{
	public event EventHandler<EventArgs> Filled;

	private MoleFieldData _moleField;
	private OccupancyLUT _occupancy;
	private MolePositionLUT _moles;

	public MoleFieldController(MoleFieldData moleField)
	{
		DEBUG.Assert(moleField != null);
		_moleField = moleField;

		_occupancy = new OccupancyLUT(_moleField.Positions.Count());
		_moles = new MolePositionLUT(_moleField.Positions.Count());

		_moleField.Positions.ForEach(x => _occupancy.Add(x, false));
		//_moles kept empty on purpose
	}

	public void Occupy(UnityEngine.Vector3 position, GameObject mole)
	{
		DEBUG.Assert(_moleField.Positions.Contains(position));
		DEBUG.Assert(mole != null);
		DEBUG.Assert(_occupancy[position] == false);

		_moles.Add(position, mole);
		_occupancy[position] = true;

		NotifyIfFilled();
	}

	public void Free(UnityEngine.Vector3 position)
	{
		DEBUG.Assert(_moleField.Positions.Contains(position));

		if (_occupancy[position])
		{
			_moles.Remove(position);
			_occupancy[position] = false;
		}
	}

	public GameObject Get(UnityEngine.Vector3 position)
	{
		DEBUG.Assert(_moleField.Positions.Contains(position));

		if (_occupancy[position])
		{
			return _moles[position];
		}

		return null;
	}

	public Vector3 GetPosition(GameObject mole)
	{
		if (_moles.ContainsValue(mole))
		{
			var selection =
			    from m in _moles
			    where m.Value == mole
			    select m.Key;
			return selection.ToArray()[0];
		}

		return Vector3.zero;
	}

	public bool IsOccuppied(UnityEngine.Vector3 position)
	{
		DEBUG.Assert(_moleField.Positions.Contains(position));
		return _occupancy[position];
	}

	public bool IsFree()
	{
		return _occupancy.All(kv => kv.Value == false);
	}

	public bool IsFull()
	{
		return _occupancy.All(kv => kv.Value == true);
	}

	private void NotifyIfFilled()
	{
		if (IsFull())
		{
			var handler = Filled;

			if (handler != null)
			{
				handler(this, new EventArgs());
			}
		}
	}

	public Vector3 GetNextFreePosition()
	{
		var freePos = GetFreePositions();

		if (freePos.Count() > 0)
		{
			return freePos[0];
		}

		return Vector3.zero;
	}

	public Vector3 GetRandomFreePosition(System.Random random)
	{
		var freePos = GetFreePositions();

		if (freePos.Count() > 0)
		{
			var rndVal = random.Next(0, freePos.Count() - 1);
			return freePos[rndVal];
		}

		return Vector3.zero;
	}

	public List<Vector3> GetFreePositions()
	{
		var oqp =
		    from o in _occupancy
		    where o.Value == false
		    select o.Key;
		return oqp.ToList();
	}

}
