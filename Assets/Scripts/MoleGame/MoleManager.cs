﻿using UnityEngine;
using System.Collections.Generic;
using System.Diagnostics;
using System;

/// <summary>
/// Mole manager.
/// manages the moles: creation, placement
/// </summary>
public class MoleManager : MonoBehaviour
{
	public event EventHandler<EventArgs> Scored;
	public event EventHandler<EventArgs> Filled;

	/// <summary>
	/// mole set data to use for this level
	/// </summary>
	public MoleSet moleSet;

	/// <summary>
	/// mole field data to use for this level
	/// </summary>
	public MoleFieldData moleField;
	private MoleFieldController _fieldController;

	private System.Random _random;

	/// <summary>
	///	Use this for initialization
	/// </summary>
	void Start()
	{
		_random = new MersenneTwister(0); //System.Time

		_fieldController = new MoleFieldController(moleField);
		_fieldController.Filled += OnMoleFieldFilled;
	}

	/// <summary>
	/// spawns a new mole from the prefab
	/// </summary>
	public void SpawnMole()
	{
		if (_fieldController.IsFull())
		{
			return;
		}

		// spawn new GameObject
		var rndDataIndex = _random.Next(0, moleSet.moles.Count);
		var prefab = moleSet.moles[rndDataIndex];
		System.Diagnostics.Debug.Assert(prefab);

		var instance = (GameObject)Instantiate(prefab);
		System.Diagnostics.Debug.Assert(instance);


		instance.transform.position = new Vector3(
		    (float)_random.Next(-5, 5),
		    -12.0f,
		    (float)_random.Next(-5, 5)
		);

		((Tappable)instance.GetComponent<Tappable>()).Tapped += OnMoleTapped;
		((MoleController)instance.GetComponent<MoleController>()).Killed += OnMoleKilled;

		var rndEmptyPos = _fieldController.GetRandomFreePosition(_random);
		_fieldController.Occupy(rndEmptyPos, instance);
	}

	/// <summary>
	/// Raises the mole killed event.
	/// </summary>
	/// <param name="sender">Sender.</param>
	/// <param name="e">E.</param>
	protected virtual void OnMoleKilled(System.Object sender, EventArgs e)
	{
		var senderController = (MoleController)sender;
		var senderGameObject = (GameObject)senderController.gameObject;
		var killedMolePos = _fieldController.GetPosition(senderGameObject);
		_fieldController.Free(killedMolePos);

		DestroyObject((GameObject)senderGameObject);
		//Score();
	}

	/// <summary>
	/// Raises the mole tapped event.
	/// </summary>
	/// <param name="sender">Sender.</param>
	/// <param name="e">E.</param>
	protected virtual void OnMoleTapped(System.Object sender, EventArgs e)
	{
		Score();
	}

	private void Score()
	{
		var handler = Scored;

		if (handler != null)
		{
			handler(this, new EventArgs());
		}
	}


	protected virtual void OnMoleFieldFilled(System.Object sender, EventArgs e)
	{
		NotifyMoleFieldFilled();
	}

	private void NotifyMoleFieldFilled()
	{
		var handler = Filled;

		if (handler != null)
		{
			handler(this, new EventArgs());
		}
	}

	///	<summary>
	/// Update is called once per frame
	/// </summary>
	void Update()
	{
		//! @TODO given a frequency, spawn a mole every n seconds
		// either here or via coroutine/event from main game
	}
}
