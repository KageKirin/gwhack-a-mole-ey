﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Mole field data.
/// holds a number of possible positions for moles to come out
/// </summary>
public class MoleFieldData : ScriptableObject
{
	/// <summary>
	/// positions that moles can come out from
	/// </summary>
	public List<Vector3> Positions;	
}
