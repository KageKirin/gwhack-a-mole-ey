﻿using UnityEngine;
using System.Collections;
using System;



/// <summary>
/// Mole controller.
/// main controller for 1 mole
/// </summary>
public class MoleController : MonoBehaviour
{
	public event EventHandler<EventArgs> Killed;

	public delegate void Action();
	public delegate void Finish(System.Object Sender, EventArgs e);

	public Timer createTimer()
	{
		return gameObject.AddComponent<Timer>();
	}

	public enum MovementState { Spawned, Underground, MovingUp, Walking, MovingDown, EndState };
	public class MoleState
	{
		public MovementState state;
		public Timer timer;
		public Action action;

		public MoleState(
			object obj,
			MovementState _state,
		    float _time,
		    Action _action,
		    Finish _finish)
		{
			state = _state;
			timer = (obj as MoleController).createTimer();
			timer.Countdown = _time;
			timer.Reset();
			timer.Finished += new EventHandler<EventArgs>(_finish);

			action = _action;
		}
	}
	public MoleState _currentState;

	public MoleData moleData;
	private MoleData _moleDataInstance;
	public Collider personalSpace;
	
	private System.Random _random;


	void Awake()
	{
		_moleDataInstance = (MoleData)MoleData.Instantiate(moleData);
		_random = new MersenneTwister();
		_currentState = new MoleState(this,
		                              MovementState.Spawned,
		                              0.01f,
		                              OnSpawned,
		                              OnSpawnFinished);
	}

	void Start()
	{
		((Tappable)gameObject.GetComponent<Tappable> ()).Tapped += OnMoleTapped;
	}

	void Update()
	{
		if(_currentState != null)
		{
			var action = _currentState.action;
			if(action != null)
			{
				action();
			}
		}
	}

	#region tap interaction
	protected virtual void OnMoleTapped(System.Object sender, EventArgs e)
	{
		Debug.Log("event OnMoleTapped - from " + sender.GetType().ToString() + " to " + this.name);
		
		_moleDataInstance.hitPoints--;
		
		if (_moleDataInstance.hitPoints <= 0)
		{
			Die();
		}
	}
	
	private void Die()
	{
		var handler = Killed;
		
		if (handler != null)
		{
			handler(this, new EventArgs());
		}
	}
	#endregion tap interaction


	#region activity
	void OnSpawned()
	{

	}
	protected void OnSpawnFinished(System.Object Sender, EventArgs args)
	{
		_currentState = new MoleState(this,
		                              MovementState.Underground,
		                              0.02f,
		                              OnUnderground,
		                              OnUndergroundFinished);
	}

	void OnUnderground()
	{
		
	}
	protected void OnUndergroundFinished(System.Object Sender, EventArgs args)
	{
		_currentState = new MoleState(this,
		                              MovementState.MovingUp,
		                              _moleDataInstance.moveInTime,
		                              OnMovingUp, OnMovingUpFinished);
	}

	void OnMovingUp()
	{
		var curve = _moleDataInstance.moveInCurve.Evaluate(_currentState.timer.RelativeProgress);
		UnityEngine.Debug.Log("progress: " + curve);

		var pos = transform.position;
		pos.y = -5.0f + 5 * curve;
		transform.position = pos;
	}
	protected void OnMovingUpFinished(System.Object Sender, EventArgs args)
	{
		_currentState = new MoleState(this,
		                              MovementState.Walking,
		                              _moleDataInstance.displayTime,
		                              OnWalking,
		                              OnWalkingFinished);
	}

	void OnWalking()
	{
		//rotation
		var angle = (float)_random.Next(
			-(int)_moleDataInstance.randomMovementArc / 2,
			(int)_moleDataInstance.randomMovementArc / 2);
		transform.Rotate(0, angle, 0,
		                 Space.Self);

		//translation
		transform.Translate(_moleDataInstance.speed * Time.deltaTime, 0, 0,
		                    Space.Self);
	}
	protected void OnWalkingFinished(System.Object Sender, EventArgs args)
	{
		_currentState = new MoleState(
			this,
			MovementState.MovingDown,
		                              _moleDataInstance.moveOutTime,
		                              OnMovingDown, OnMovingDownFinished);
	}

	void OnMovingDown()
	{
		var curve = _moleDataInstance.moveOutCurve.Evaluate(_currentState.timer.RelativeProgress);
		UnityEngine.Debug.Log("progress: " + curve);
		var pos = transform.position;
		pos.y = 0.0f - 5 * curve;
		transform.position = pos;
	}
	protected void OnMovingDownFinished(System.Object Sender, EventArgs args)
	{
		_currentState = new MoleState(this,
		                              MovementState.EndState,
		                              0.02f,
		                              OnEndState, OnEndStateFinished);
	}

	void OnEndState()
	{
		
	}
	protected void OnEndStateFinished(System.Object Sender, EventArgs args)
	{
		Die();
	}
	#endregion activity

	#region collision avoidance
	void OnCollisionEnter(Collision collision)
	{
		//collision.collider;
		var collidedMoleController = collision.gameObject.GetComponent<MoleController>() as MoleController ;
		if(collidedMoleController != null)
		{
			if(collidedMoleController._currentState.state == MovementState.Walking)
			{
				//! @TODO: avoid
				transform.Rotate(0, 180, 0,
				                 Space.Self);
			}
		}
	}
	#endregion collision avoidance
}


/*
// TODO move to UpdateInternalStateOverTime;
_internalTime += Time.deltaTime;
var progress = 0.0f;

switch (_movementState)
{
case MovementState.MovingIn:
	progress = _internalTime / (_moleDataInstance.moveInTime + 0.000001f);
	
	if (progress >= 1.0f)
	{
		_movementState = MovementState.Display;
		_internalTime = 0.0f;
	}
	
	break;
	
case MovementState.Display:
	progress = _internalTime / (_moleDataInstance.displayTime + 0.000001f);
	
	if (progress >= 1.0f)
	{
		_movementState = MovementState.MovingOut;
		_internalTime = 0.0f;
	}
	
	break;
	
case MovementState.MovingOut:
	progress = _internalTime / (_moleDataInstance.moveOutTime + 0.000001f);
	
	if (progress >= 1.0f)
	{
		return;
	}
	
	break;
}

// TODO move to UpdateAction
AnimationCurve updateCurve = null;

switch (_movementState)
{
case MovementState.MovingIn:
	updateCurve = _moleDataInstance.moveInCurve;
	break;
	
case MovementState.Display:
	updateCurve = _moleDataInstance.displayCurve;
	break;
	
case MovementState.MovingOut:
	updateCurve = _moleDataInstance.moveOutCurve;
	break;
}

if (updateCurve == null)
{
	updateCurve = AnimationCurve.Linear(0, 0, 1, 1);
}

var pos = transform.position;
pos.y = updateCurve.Evaluate(Time.time) * 1.0f;
transform.position = pos;
*/