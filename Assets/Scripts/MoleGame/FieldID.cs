﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// FieldID
/// </summary>
public class FieldID : typedef<int>
{
	#region Constructor
	public FieldID()
	: base()
	{ }

	public FieldID(int value)
	: base(value)
	{ }

	public FieldID(FieldID value)
	: base(value.m_value)
	{ }
	#endregion
}
