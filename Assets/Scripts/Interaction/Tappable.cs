﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Tappable.
/// register a tap on object and sends event
/// </summary>
public class Tappable : MonoBehaviour
{
	public event EventHandler<EventArgs> Tapped;

	private bool _tapSent = false;
	private bool tapSent
	{
		get { return _tapSent; }
		set
		{
			_tapSent = value;
			var handler = Tapped;

			if (value && handler != null)
			{
				handler(this, new EventArgs());
			}
		}
	}


	void OnMouseDown()
	{
		Debug.Log("OnMouseDown" + this.name);

		if (!tapSent)
		{
			tapSent = true;
		}
	}

	void OnMouseUp()
	{
		Debug.Log("OnMouseUp" + this.name);
		tapSent = false;
	}

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

	}
}
