﻿using UnityEngine;
using System;

/// <summary>
/// a simple countdown timer
/// event at finish
/// </summary>
public class Timer : MonoBehaviour
{
	public event EventHandler<EventArgs> Finished;

	public float Countdown = 1.0f;
	public float RemainingTime { get; private set; }
	public float RelativeProgress
	{
		get
		{
			return 1.0f - Mathf.Clamp(RemainingTime / (Countdown + 0.000001f), 0.0f, 1.0f);
		}
	}

	void Start()
	{
		Reset();
	}

	void Update()
	{
		if (RemainingTime > 0.0f)
		{
			RemainingTime -= Time.deltaTime;

			if (RemainingTime <= 0.0f)
			{
				NotifyFinished();
			}
		}
	}

	void NotifyFinished()
	{
		var handler = Finished;

		if (handler != null)
		{
			handler(this, new EventArgs());
		}
	}

	public void Reset()
	{
		RemainingTime = Countdown;
	}
}
