﻿using UnityEngine;
using System.Collections;
using DEBUG = System.Diagnostics.Debug;

/// <summary>
/// Time display.
/// displays the remaining time
/// </summary>
public class TimeDisplay : DisplayElement

{
	private float _time = 0;
	public uint Time { set { _time = value;} }

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void OnGUI()
	{
		DEBUG.Assert(Position != null);
		GUI.skin = Skin;
		GUI.Label(Position, "Time:" + _time.ToString());
	}
}
