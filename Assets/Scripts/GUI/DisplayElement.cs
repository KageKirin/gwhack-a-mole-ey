using UnityEngine;
using System.Collections;
using DEBUG = System.Diagnostics.Debug;

/// <summary>
/// Base display element.
/// displays
/// </summary>
public class DisplayElement : MonoBehaviour
{
	public Rect Position = new Rect(0, 20, 100, 20);
	public GUISkin Skin;
}
