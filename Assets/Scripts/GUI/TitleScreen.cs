using UnityEngine;
using System.Collections;
using MoleGUI;

namespace MoleGUI
{
	public class TitleScreen : FullScreenButton
	{
		override public void OnPressAction()
		{
			Application.LoadLevel("MenuScreen");
		}
	}
}