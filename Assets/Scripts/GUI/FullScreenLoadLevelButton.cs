using UnityEngine;
using System.Collections;
using MoleGUI;

namespace MoleGUI
{
	public class FullScreenLoadLevelButton : FullScreenButton
	{
		public string LevelName;

		override public void OnPressAction()
		{
			Application.LoadLevel(LevelName);
		}
	}
}