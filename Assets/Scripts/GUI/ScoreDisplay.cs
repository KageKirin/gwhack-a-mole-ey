﻿using UnityEngine;
using System.Collections;
using DEBUG = System.Diagnostics.Debug;

/// <summary>
/// Score display.
/// displays the score
/// </summary>
public class ScoreDisplay : DisplayElement
{
	private uint _score = 0;
	public uint Score { set { _score = value;} }

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void OnGUI()
	{
		DEBUG.Assert(Position != null);
		GUI.skin = Skin;
		GUI.Label(Position, "Score:" + _score.ToString());
	}
}
