using UnityEngine;
using System.Collections;

namespace MoleGUI
{
	public class FullScreenButton : DisplayElement
	{
		public Texture2D ButtonTexture;

		void Start()
		{
			Position = new Rect(0, 0, Screen.width, Screen.height);
		}

		private void OnGUI()
		{
			GUI.skin = Skin;
			if (GUI.Button(Position, ButtonTexture))
			{
				OnPressAction();
			}
		}

		public virtual void OnPressAction()
		{

		}
	}
}