using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class UnityExtensions
{
	public static GameObject GetFirstChildWithTag(this GameObject self, string tag)
	{
		if (tag == null)
		{
			throw new ArgumentNullException("tag");
		}

		foreach (Transform t in self.transform)
		{
			if (t.gameObject.CompareTag(tag))
			{
				return t.gameObject;
			}

			t.gameObject.GetFirstChildWithTag(tag);
		}

		return null;
	}

	public static GameObject[] GetChildrenWithTag(this GameObject self, string tag)
	{
		if (tag == null)
		{
			throw new ArgumentNullException("tag");
		}

		List<GameObject> results = new List<GameObject>();

		foreach (Transform t in self.transform)
		{
			if (t.gameObject.CompareTag(tag))
			{
				results.Add(t.gameObject);
			}

			results.AddRange(t.gameObject.GetChildrenWithTag(tag));
		}

		return results.ToArray();
	}

	public static GameObject GetParentWithTag(this GameObject self, string tag)
	{
		if (tag == null)
		{
			throw new ArgumentNullException("tag");
		}

		for (var t = self.transform; t != null; t = t.parent)
		{
			if (t.CompareTag(tag))
			{
				return t.gameObject;
			}
		}

		return null;
	}
}