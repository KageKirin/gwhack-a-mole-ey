using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public static class Extensions
{
	/// <summary>
	/// Performs the specified action on each element of the System.Collections.Generic.IEnumerable<T>.
	/// </summary>
	///
	/// <param name="action">
	/// The System.Action<T> delegate to perform on each element of the System.Collections.Generic.IEnumerable<T>.
	/// </param>
	///
	/// <exception cref="System.ArgumentNullException">
	/// action is null.
	/// </exception>
	///
	public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
	{
		if (action == null)
		{
			throw new ArgumentNullException("action");
		}

		foreach (var e in source)
		{
			action(e);
		}
	}


	public static int FirstEmptyIndex<T>(this List<T> source)
	{
		if (source.Capacity == 0)
		{
			return -1;
		}

		if (source.Count == 0)
		{
			return 0;
		}

		for (int i = 0; i < source.Capacity; i++)
		{
			if (source[i] == null)
			{
				return i;
			}
		}

		return -1;
	}
}